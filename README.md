## 4 in a row AI
4 in a row AI based on Duelling Double Deep Q-Learning

## Description
This project should train an AI which plays on a human level or above using Duelling Double Deep Q-Learning as an Deep Reinforcement Learning algorithm.

## Installation
Install:
- python 3.9 or above
Libraries:
- pytorch
- numpy
- gym

## Usage
Use 'python3 game.py' in the same directory as this whole project is. Then you can play the game. If AIs are wanted just run the Trainingsprocess and put your best agent as 'best_agent.pt' into a folder 'trained_models'.
This is an code example, so feel free to play around with the program.

## Authors
AirFlow

## License
Just do not copy the program and say that it is yours.

## Project status
More or less finished

## Lessons Learned
**Biggest insight:**
The Q-Values are always overestimated resulting in the AI learning nothing serious. That can be solved by using Q-Learning variances that solve this problem. The issue is especially big as the reward is sparse and therefore the NN gets updated by a lot of "TD Target" and less of real returns. -> Less samples fix the AI into the range [min_reward; max_reward] and the other ones get slowly overestimated out of this range causing the NN to overestimates even the end states and returns.

(But of course I did this in my second semester)

**What also had been better:**
Make this setup like any other "Reinforcement Learning Environment Agent Style". Right now the game uses the AI and the other way around because I started this with the idea of two AIs training at the same time but this is not needed. -> Better idea (what I nearly implemented at the end): make the environment the game + an Agent which plays. Therefore, one can build the classical Environment - Agent architecture and use every RL algorithm. The Agent which is within the Environment can at the beginning be a random playing agent. Once the trained Agent beats this agent, one can fix the trained Agent and set it as the opponent within the Environment and let the agent learn how to play against that agent.

The AI should also be trained as Player 2. Obviously because this can also happen.

I will probably use all of this knowledge and lessons learned to train my own chess AI.
