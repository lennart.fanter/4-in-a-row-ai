import time

class Stopwatch():
    def __init__(self) -> None:
        self.start = 0
        self.stop = 0

def start(self):
    self.start = time.time()

def end(self):
    self.stop = time.time()

def give_last_time(self):
    time_convert(self.stop-self.start)

def time_convert(sec):
    mins = sec // 60
    sec = sec % 60
    hours = mins // 60
    mins = mins % 60
    print("Time Lapsed = {0} hours {1} minutes {2} seconds".format(int(hours),int(mins),sec))