import torch
import torch.nn as nn
import numpy as np
import random
import copy
import player
import ReplayMemory
import ExploitationVsExploration

from global_param import empty_space
from global_param import field_dim

# data: (state, next_state, action, reward, done)
state_index = 0
next_state_index = 1
action_index = 2
reward_index = 3
done_index = 4

class Agent(player.Player, nn.Module):
    def __init__(self, idN, name, dimension):
        super().__init__(idN, name)
        nn.Module.__init__(self)

        self.eval_epsilon = []
        self.eval_action_taken = []
        self.eval_squarred_mean_td_target = []

        self.episode = 0
        rows,columns = dimension
        self.experience_buffer = []
        self.num_act = columns
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    

        #training parameter --- changeable for better resulsts, maybe
        maxExplorationRate = 0.8
        minExplorationRate = 0.07
        explore_at_end_frac = 0.8

        memorycapacity = 1000

        self.update_rate = 1 # after each x'th episode
        self.target_update_rate = 6 # after each x'th episode
        self.mini_batchsize = 70
        self.gamma = 0.99
        self.learning_rate = 0.001

        self.q_network = nn.Sequential(
            nn.Linear(np.array(dimension).prod(), 64), 
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Linear(64, self.num_act),
        )

        self.target_network = copy.deepcopy(self.q_network)

        
        self.explore_object = ExploitationVsExploration.ExploitationVsExploration(max=maxExplorationRate,min=minExplorationRate,explore_at_end_frac=explore_at_end_frac)
        self.replay_memory = ReplayMemory.ReplayMemory(memorycapacity)
        self.optimizer = torch.optim.Adam(self.q_network.parameters(), lr=self.learning_rate)



    def get_q_values(self, x):
        return self.q_network(x)

    def get_target_values(self, x):
        return self.target_network(x)

    def update_target_network(self):
        self.target_network.load_state_dict(self.q_network.state_dict())

    def compute_TD_target(self, data):
        with torch.no_grad():
            target_max, _ = self.get_target_values(data[next_state_index]).max(dim=1) 
            td_target = data[reward_index].flatten() + self.gamma * target_max * (1 - data[done_index].flatten())

        return td_target


    def compute_TD_target_anti_overestimation(self, data):
        with torch.no_grad():
            _, q_value_argmax = self.get_q_values(data[next_state_index]).max(dim=1,keepdim = True)
            
            target = self.get_target_values(data[next_state_index])

            target_at_q_argmax = torch.gather(target,-1,q_value_argmax)[:,:1]
            target_at_q_argmax = target_at_q_argmax.reshape((self.mini_batchsize,))
        
            td_target = data[reward_index].flatten() + self.gamma * target_at_q_argmax * (1 - data[done_index].flatten())

        return td_target

    def update(self, episode):
        if episode % self.update_rate == 0 and self.replay_memory.can_provide_sample(self.mini_batchsize):
            experiences = self.replay_memory.sample(self.mini_batchsize)
            
            # td_target = self.compute_TD_target(experiences)
            td_target = self.compute_TD_target_anti_overestimation(experiences)

            # calculate loss between target and value
            q_values = self.get_q_values(experiences[state_index])
            old_val = q_values.gather(1, experiences[action_index]).squeeze()

            if (old_val**2).mean() > 25 or (td_target**2).mean() > 25:
                pass 
                # print("something fishy")

            self.eval_squarred_mean_td_target.append((td_target**2).mean())

            loss_helper = nn.MSELoss() 
            loss = loss_helper(old_val, td_target)

            # optimize the model
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

        # update the target network
        if episode % self.target_update_rate == 0:
            self.update_target_network()



    def take_action(self, state, greedy=True):
        state_loc =  copy.deepcopy(state)
        state_loc = self.pre_process(state_loc)
        explore = self.explore_object.linear_eps(self.episode)
        if not greedy and explore >= random.random():
            action = np.array([random.choice(range(self.num_act))])
        else:
            q_values = self.get_q_values(torch.Tensor(state_loc).to(self.device))
            action = torch.argmax(q_values, dim=0).cpu().numpy()

        action = action.item()
        if not greedy:
            self.eval_epsilon.append(explore)
            self.eval_action_taken.append(action)
            self.get_experience((state_loc, action))   
        return action
    


    """
        experience is either state, action or next_state, reward, done
        make it to: state, next_state, action, reward, done
    """
    def get_experience(self, experience):
        if len(experience) == 2:
            self.experience_buffer.append(self.pre_process(copy.deepcopy(experience[0])))
            self.experience_buffer.append(experience[1])
        elif len(experience) == 3:
            self.replay_memory.push((self.experience_buffer[0], self.pre_process(copy.deepcopy(experience[0])), self.experience_buffer[1], experience[1], experience[2]))
            self.experience_buffer = []

    """
        the neural network should always get the same input meaning its own entries should be always 1 and those of the oponent -1
    """
    def pre_process(self,state):
        global empty_space
        state_local =  copy.deepcopy(state)
        if state_local is None:
            field = np.zeros(field_dim) * -11
            return torch.tensor(field.flatten(), dtype=torch.float)
        if isinstance(state_local, torch.Tensor):
            return state_local
        
        state_local[state_local == float(self.id)] = 1.
        opponent_id = 2. if self.id == 1 else 1.
        state_local[state_local == opponent_id] = -1.
        state_local[state_local == empty_space] = 0.

        return torch.tensor(state_local.flatten()).float()
    
    def copy_behaviour_network(self, other_agent):
        self.q_network.load_state_dict(other_agent.q_network.state_dict())

    """
        evals:
            epsilon
            action taken

    """
    def eval(self):
        return self.eval_epsilon,self.eval_action_taken, self.eval_squarred_mean_td_target
    
    

def load_ai() -> Agent:
    return torch.load(f"ai_.pth")

if __name__ == "__main__":
    test_agent = Agent(1, "Boo", field_dim)
    test_pre_process = np.array([1,2,0,1,2,0,1,2,0]).reshape(3,3)
    assert torch.tensor(np.array([1,-1,0,1,-1,0,1,-1,0])) == test_agent.pre_process(test_pre_process)
    