import copy
import game as g
import ai
import Stopwatch
import numpy as np
import matplotlib.pyplot as plt
import random_bot
import helper

from datetime import datetime

from global_param import games


def show_eval(no_wins = None, eps = None, actions = None, td_targets = None):
    if not no_wins is None:
        plt.plot(range(len(no_wins)), no_wins)
        plt.ylabel("Wins")
        plt.xlabel("episode")
        plt.show()

        input("Press enter")

    if not eps is None:
        plt.plot(range(len(eps)), eps)
        plt.ylabel("Eps")
        plt.xlabel("Step")

        plt.show()

        input("Press enter")

    if not actions is None:
        plt.plot(range(len(actions)), actions )
        plt.ylabel("Action")
        plt.xlabel("step")
        
        plt.show()

        input("Press enter")

    if not td_targets is None:
        plt.plot(range(len(td_targets)), td_targets )
        plt.ylabel("td targets squarred mean")
        plt.xlabel("step")
        
        plt.show()

        input("Press enter")


"""
    return: evaluation Tuple of list (no. wins, no. game),
"""
def train_agent(bot1, bot2, games, opponent_run, time_string):

    clock = Stopwatch.Stopwatch()

    ai_train = True
    save_model = True
    no_one_wins_evaluation = []
    number_one_wins = 0

    Stopwatch.start(clock)
    for i in range(games):
        if i> games - 10 or i<3:
            winner = g.start_new_game(np.zeros((6,7)),bot1,bot2,True, train=True)
        else:
            winner = g.start_new_game(np.zeros((6,7)),bot1,bot2,False, train=True)
        
        if winner == 1:
            number_one_wins += 1

        if ai_train:
            if winner == 1:
                bot1.get_experience((None,5,True)) 
            elif winner == 2:
                bot1.get_experience((None,-5,True))
            else: 
                bot1.get_experience((None,0,True))

            bot1.update(i)
            bot1.episode += 1

            no_one_wins_evaluation.append(number_one_wins)

    print("Number one wins: "+str(number_one_wins))
    Stopwatch.end(clock)
    Stopwatch.give_last_time(clock)

    if save_model:
        helper.save_ai(bot1,f"AI_RUN{opponent_run}",time_string,True)

    return no_one_wins_evaluation





if __name__ == "__main__":
    opponent_updates = 15
    opponent_id = 2
    opponent_name = "Bernd"

    now = datetime.now()
    time_string = now.strftime("%Y-%m-%d_%H:%M:%S")

    show_evaluation = False

    bot1 = ai.Agent(1,"Boo",(6,7))
    bot2 = random_bot.RandomBot(opponent_id, opponent_name)

    end_eval = []

    for i in range(opponent_updates):
        bot1.episode = 0
        print(f"start run {i}")

        no_wins_eval = train_agent(bot1, bot2, games, i, time_string)
        end_eval.append(no_wins_eval)

        # adapt the bot so it does not start completely random for the new opponnent
        # bot1.explore_object.max = 0.4

        # could also use copy_behaviour_network
        bot2 = copy.deepcopy(bot1)
        bot2.id = opponent_id
        bot2.name = opponent_name

    print("end training")

    if show_evaluation:
        for eval in end_eval:
            show_eval(no_wins=eval)
    eps, actions, td_targets = bot1.eval()
    show_eval(actions=actions, eps=eps, td_targets=td_targets)
