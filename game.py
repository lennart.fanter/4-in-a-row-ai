import helper
import player
import ai
import numpy as np
import global_param

import helper

# the field is seen from above to the bottom so row 0 is the top row


#defines
empty_space = global_param.empty_space
pieces_in_a_row_to_win = 4

#returns the player ID if the player won by his last move
#returns 0 else
def is_finished(table,column):
    global pieces_in_a_row_to_win
    rows,columns = table.shape
    row = 0

    # search #current PlayerID/Number
    while table[row][column] == empty_space:
        row += 1
        if row == rows: #should not happen if is_finished is called with the same column where the player played his move
            return 0
    playerID = table[row][column]

    #check if pieces_in_a_row_to_win in the same column
    i = 1
    while i+row < rows and playerID == table[row+i][column]:
        i += 1
    if i > pieces_in_a_row_to_win-1:
        return playerID 

    #check if pieces_in_a_row_to_win in the same row
    right = 1
    while right+column < columns and  playerID == table[row][column+right]:
        right += 1
    left = 1
    while column-left >= 0 and  playerID == table[row][column-left]:
        left += 1
    if left + right > pieces_in_a_row_to_win:
        return playerID 
    
    #check if pieces_in_a_row_to_win at 135 degree in a row
    right = 1
    while right+row < rows and right+column < columns and  playerID == table[row+right][column+right]:
        right += 1
    left = 1
    while row-left >= 0 and column-left >= 0 and  playerID == table[row-left][column-left]:
        left += 1
    if left + right > pieces_in_a_row_to_win:
        return playerID

    #check if pieces_in_a_row_to_win at 45 degree in a row
    right = 1
    while right+row < rows and column-right >= 0 and  playerID == table[row+right][column-right]:
        right += 1
    left = 1
    while column+ left < columns and rows - left >= 0 and playerID == table[row-left][column+left]:
        left += 1
    if left + right > pieces_in_a_row_to_win:
        return playerID
    
    return 0


def init_game():
    global empty_space
    rows = global_param.field_dim[0]
    columns = global_param.field_dim[1]
    print("Bitte geben Sie die Anzahl der menschlichen Spieler an: ")
    num_player = helper.intIO(0,2)
    i = 0
    players = []
    while num_player > i:
        name = input(f"Bitte geben Sie den Namen von Spieler{i+1} ein: ")
        print("")
        players.append(player.Player(i+1,name))
        i += 1
    while len(players) < 2:
        name = "Boo" if len(players) == 0 else "Bernd"
        bot = helper.load_model("/Users/lennartfanter/Desktop/Programmier_Projekte/4-in-a-row-ai/trained_models/best_model.pt")
        bot.id = len(players) + 1
        bot.name = name
        players.append(bot)


    table = np.zeros((rows,columns))
    table = table + empty_space
        
    return table,players[0],players[1],True



#does not catch errors regarding wrong inputs
#returns the new table and a boolean if the move was correct
def check_move_and_play_it(table,column,playerID):
    rows,columns = table.shape
    i = 0
    while i < rows and table[i][column] == empty_space:
        i += 1
    if i == 0:
        return table,False
    table[i-1][column] = playerID
    return table,True


def give_experience(bot,next_state, reward, done):
    ai.Agent.get_experience(bot,(next_state, reward, done))




"""
    who_starts: 1 = player1, 2 = player2
"""
def start_new_game(field,player1,player2,show, train = False, who_starts = 1):
    #init
    rows,columns = field.shape
    maxRounds = rows*columns
    caption = "  "
    for i in range(columns):
        caption += str(i+1) + "  "

    finished = False
    round = 0
    #init end

    #print Start of game
    if show:
        print("Spieler1: "+player1.name)
        print("      vs.")
        print("Spieler2: "+player2.name)
        print("")

    #game 'loop'
    while not finished:
        if (round + who_starts + 1) % 2 == 0:
            aktP = player1
        else: 
            aktP = player2

        # give ai the experience of the last move - which can only be done if opponent has played - not in the first round of ai
        if train and isinstance(aktP,ai.Agent) and aktP == player1 and round > who_starts:
            give_experience(aktP,field,0,False)

        if show:
            print(caption)
            print(field)
            print("")


        #play a move
        tries = 0
        succeded = False
        while not succeded and tries < 5:
            if train and isinstance(aktP,ai.Agent) and aktP == player1:
                column = aktP.take_action(field,greedy=False)
            else:
                column = aktP.take_action(field)
            field,succeded = check_move_and_play_it(field,column,aktP.id)
            if not succeded and not train:
                print("Inkorrekte Eingabe Spalte ist bereits voll.")
                print("")
            tries += 1
        
        #incorrect move
        if tries == 5:
            finished = True
            if show:
                print(aktP.name+" hat fünf mal hintereinander einen ungültigen Zug versucht zu spielen.")
                print("")
            if aktP == player1:
                aktP = player2
            else:
                aktP = player1
        #end play move

        finished = True if finished or is_finished(field,column) != 0 or round >= maxRounds else False
        round += 1
    #game 'loop' end
    
    #game end show result
    if show:
        print(caption)
        print(field)
        print("")
        if round == maxRounds:
            print("Unentschieden.")
        else:
            print(aktP.name+" hat gewonnen!")
        print("")
    
    if round == maxRounds:
        return -1
    return aktP.id #return winner id


if __name__ == "__main__":
    field,player1,player2,show = init_game()
    start_new_game(field,player1,player2,show)