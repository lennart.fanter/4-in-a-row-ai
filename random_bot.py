import random
import player


class RandomBot(player.Player):
    def __init__(self, idN, name):
        super().__init__(idN, name)

    def take_action(self, table):
        rows, num_act = table.shape
        return random.choice(range(num_act))