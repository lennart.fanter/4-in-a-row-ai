import random
import torch

class ReplayMemory():
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.push_count = 0

    def push(self, experience):
        if self.push_count < self.capacity:
            self.memory.append(experience)
        else:
            self.memory[self.push_count % self.capacity] = experience
        self.push_count += 1

    def sample(self, batch_size):
        random_sample = random.sample(self.memory, batch_size)
        return unpack_tensors(random_sample)
    

    def can_provide_sample(self, batch_size):
        return len(self.memory) >= batch_size

def unpack_tensors(tuples_list):
    states, next_states, actions, rewards, dones = zip(*tuples_list)
 
    states_tensor = torch.stack(states)
    next_states_tensor = torch.stack(next_states)
    actions_tensor = torch.tensor(actions, dtype=torch.long).unsqueeze(1)
    rewards_tensor = torch.tensor(rewards, dtype=torch.float32)
    dones_tensor = torch.tensor(dones, dtype=torch.float32) 

    return states_tensor, next_states_tensor, actions_tensor, rewards_tensor, dones_tensor


# unit test
if __name__ == "__main__":
    tuples = [
    (torch.tensor([1.0, 2.0]), torch.tensor([1.1, 2.1]), 0, 1.0, False),
    (torch.tensor([3.0, 4.0]), torch.tensor([3.1, 4.1]), 1, 0.5, True)
    ]
    states_tensor, next_states_tensor, actions_tensor, rewards_tensor, dones_tensor = unpack_tensors(tuples_list=tuples)

    print("States Tensor:\n", states_tensor)
    print("Next States Tensor:\n", next_states_tensor)
    print("Actions Tensor:\n", actions_tensor)
    print("Rewards Tensor:\n", rewards_tensor)
    print("Dones Tensor:\n", dones_tensor)