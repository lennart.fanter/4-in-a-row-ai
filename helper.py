import torch
import os

def save_ai(model,run_name, exp_type=None, print_path=True):
        exp_folder = "" if exp_type is None else exp_type
        folder_path, _ = create_folder_relative(f"{exp_folder}/{run_name}")
        model_full_path = f"{folder_path}/agent_model.pt"
        with open(model_full_path, 'wb') as f:
            torch.save(model, f)
        if print_path:
            print(f"Agent model saved to path: \n{model_full_path}")

def make_log_dir():
    log_dir = os.path.abspath('logs')
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)
    return log_dir

def create_folder_relative(folder_name, assert_flag=False):
    """
    Creates a folder with the given name in the current directory and returns the absolute path to the folder. The current directory is wrt the directory of the notebook that calls this function
    Args:
        folder_name: A string specifying the name of the folder to create.
        assert_flag: A boolean indicating whether to raise an AssertionError if the folder already exists.
    Returns:
        A tuple containing:
        - The absolute path to the newly created folder or existing folder.
        - A boolean indicating whether the folder already existed.
    """
    log_dir = make_log_dir()
    abs_folder_path = os.path.abspath(f"{log_dir}/{folder_name}")
    if not os.path.isdir(abs_folder_path):
        if assert_flag:
            assert False, "Following folder does not exist %s" % (abs_folder_path)
        os.makedirs(abs_folder_path)
        folder_already_exist = False
    else:
        folder_already_exist = True
    return abs_folder_path, folder_already_exist

def load_model(path):
    model = torch.load(path,
                       map_location=torch.device("cuda" if torch.cuda.is_available() else "cpu"))
    return model

def intIO(start,end):
    inp = float('-inf')
    while inp < start or inp > end:
            try:
                inp = int(input("Bitte geben Sie eine Zahl zwischen (einschließlich) "+str(start)+"  und "+str(end)+" ein: "))
                print("")
            except ValueError:
                print("")
                print("Bitte eine ganze Zahl im Wertebereich eingeben.")
                print("")
                inp = float('-inf')
                continue
            
            if inp < start or inp > end:
                print("Inkorrekte Eingabe, Wert liegt nicht im Eingabebereich.")
                print("")
                continue
    return inp